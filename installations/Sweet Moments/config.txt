integer channelDialog = 123;

broadcast()
{
    // Message S0 announces the start of a new config
    // and will make objects forget any existing config
    llSay(0, "Sending out new config");
    sendMsg("S0");
    
    // Testing
    configureDoor("01", TRUE, -80.0, "interior", "02", "");
    configureDoor("02", TRUE,  80.0, "interior", "01", "");
    
    // The one-way entrance
    configureDoor("10", TRUE, -80.0, "special", "11", "");
    configureDoor("11", TRUE, -80.0, "special", "10", "01");

    // Main door
    configureDoor("12", TRUE,  80.0, "exterior", "", "");

    // Store cupboard
    configureDoor("14", FALSE,  80.0, "interior", "", "");

    // Cell doors
    configureDoor("20", TRUE,  -80.0, "interior", "21", "");
    configureDoor("21", FALSE,  80.0, "interior", "20", "");
    configureDoor("22", FALSE, -80.0, "interior", "", "");
    configureDoor("24", FALSE, -80.0, "interior", "", "");
    configureDoor("26", TRUE,  -80.0, "interior", "27", "");
    configureDoor("27", FALSE,  80.0, "interior", "26", "");


    // Visitor garden
    configureDoor("40", TRUE,  80.0, "visitor", "41", "");
    configureDoor("41", TRUE,  -80.0, "visitor", "40", "");

    sendPermission("01", "A", "*", "Y");
    sendMsg("SZ");
}
